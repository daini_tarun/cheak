﻿<?php
include($config['sitepath']."classes/pageNavigation.php");
include($config['sitepath']."classes/fileUploading.php");
include($config['sitepath']."classes/querymaker.class.php");
include($config['sitepath']."classes/error.php");
include($config['sitepath']."classes/classValidField.php");
/*
 *   Class for admin.
 */
class mainClass {
    public $attributes	= array();  // Post,Get and File Variables
    public $error 		= array();  // Array For Error To Display 
	public $config 		= array();  // Array For Basic Data Variable
	public $data 		= array();	// Array For List Data
	
	public $db 			= Null;     // Database Class Object Variable
    public $validate 	= Null;		// Validate Class Object Variable
    public $errorObj 	= Null;		// Error Class Object Variable
	
	/**
     * __construct()	    : Constructor of the class
     *
     * @param $attributes	: All post,get,file Variables     
     * @return
     */
    
    function __construct($attributes,$config) {
	
	    $this->attributes 	= $attributes;	// Assign Post,Get And File Variabls To An Array
		$this->config 		= $config;	// Assign Common Array To Class Array
        $this->validate 	= new classValidField();	// Assign Object Of Validation Class 
        $this->errorObj 	= new error();	// Assign Object Of Error Class
		$this->db 			= new CLSQueryMaker($config['dbHost'], $config['dbUser'], $config['dbPassword'], $config['dbName']);	// Assign Object Of Database Class
		
    } // END OF CONSTRUCTOR
	
    /**
     * sqlInjection() 	: Prevent sql query varibale
     *
     * @param $var		: Varibale
     * @return			: Sql Injected Varibale
     */
    
    function sqlInjection($var) {
        $var = mysql_escape_string($var);
        return $var;
    } // END OF sqlInjection()
	
	/**
	 * outputData()	: get data form table
	 * 
	 * @param $str	: Sql Injected Varibale
	 * @return 		: Original Variable
	 */
	
	function outputData($str){
		$outputData = stripslashes(trim($str));
		//$outputData = stripslashes(html_entity_decode($str));
		return $outputData;
	} // END OF outputData()
	
	/**
	 * checkSession()	: To Check The Login Session
	 */
		
	function checkSession(){
		if(!isset($_SESSION['studentName'])){
			header("Location: index.php");
		}else if($_SESSION['studentName']==''){
			header("Location: index.php");
		}
	} // END OF checkSession()
	
	/**
	 * selectOption()		: To Get Options For An Select Control
	 * 
	 * @param $table		: Name Of The Table For Which Option Created
	 * @param $optionField	: Option Display Field Name
	 * @param $valueField	: Option Value Field Name
	 * @param $selected		: Optional Parameter To Show the Selected Field
	 * @return 				: Options For Select Control
	 */
	
	function selectOption($table,$optionFiled,$valueField,$selected="",$where=""){
		
		if($selected!=""){
			$valCheck = explode(",",$selected);
		}
		
		//$where 		= "";
		$orderby 	= $optionFiled." asc";
		$option 	= "";
		
		$fetch_data=$this->db->mysqlSelect($optionFiled.",".$valueField,$this->config['tbl_prefix'].$table,$where,$orderby);
		
		for($i=0;$i<sizeof($fetch_data);$i++){
			$option .= "<option value='".$fetch_data[$i][$valueField]."'";
			
			if($selected!=""){
				if(in_array($fetch_data[$i][$valueField],$valCheck)){
					$option .= " selected='selected'";
				}
			}
			
			$option .= ">".$fetch_data[$i][$optionFiled]."</option>";
		}
		return $option;
	} // END OF selectOption()
	
	/**
	 * uploadFile()	````: To Upload File
	 * 
     * @param $path		: Image Path Where To Store It
	 * @return 			: Image Name Or Error Message
	 */
	
	function uploadFile($path,$fieldName,$type,$thubmWidth="",$thubmHeight=""){
		
		if($this->attributes[$fieldName]['name']!=''){
			$objFileUploading = new FileUploading($path,$fieldName);
			if($objFileUploading->checkFileValidity($type)){
				//if((int)$_FILES[$fieldName]['size']>(int)$this->config['MAX_FILE_SIZE']){
					$strPhotoName =  date("H-i-s_m_y_").rand(6,666);
					if(false != $objFileUploading->fn_fileUpload($strPhotoName)){
						$strPhotoName = $strPhotoName.'.'.$objFileUploading->fileExt;
						if($type=='image'){
							$objFileUploading->make_thumb($path.$strPhotoName,$path."thumbs/".$strPhotoName,$thubmWidth,$thubmHeight,$objFileUploading->fileExt);
						}
						return $strPhotoName;
					}else{
					  $this->error[] = $objFileUploading->getErrorMessage();
					  return false;
					}
				//} else {	
				//	$this->error[] = "Size of image should not be more than ".$this->config['MAX_FILE_SIZE']."kb.";
				//	return false;
				//}						   
			}else{
			   $this->error[] = $objFileUploading->getErrorMessage();
			   return false;
			}
		}else{
			$this->error[] = 'Please select a file.';
			return false;
		}
	} // END OF uploadFile()

	/**
	 * login() : login for admin
     * @return 
     */
	 

	
  
function getCategory($catId = "") {
        
		if($catId != '')
		{
		$where = " categoryId = '".$catId."'";
		}
		else {
			$where = '';
		}
        
        $orderby = " categoryId desc";
        
        $fetch_data = $this->db->mysqlSelect("*", 'tbl_category', $where, $orderby, "", "", "");
        return $fetch_data;
    }

function getPage($pageId = "") {
           $where = " pageId = '".$pageId."'";
        
        
        $fetch_data = $this->db->mysqlSelect("*", 'tbl_page', $where, $orderby, "", "", "");
        return $fetch_data;
    }

function getProduct($limit = "") {
        
            $where = "";
        
        $orderby = " productId desc";
		if ($limit == 5) {
			
             $limtby = "5";
        }
		else 
		if($limit == 10) {
			
		 		$limtby = "10";
		}
        
        $fetch_data = $this->db->mysqlSelect("*", 'tbl_product', $where, $orderby, "", "", $limtby);
        return $fetch_data;
    }
	
	
function getTestimonial() {
        
            $where = "";
        
        $orderby = " testimonialId desc";
		
			
             $limtby = "4";
       
        
        $fetch_data = $this->db->mysqlSelect("*", 'tbl_testimonial', $where, $orderby, "", "", $limtby);
        return $fetch_data;
    }

function getProDes($productId = "") {
           $where = " productId = '".$productId."'";
        
        
        $fetch_data = $this->db->mysqlSelect("*", 'tbl_product', $where, $orderby, "", "", "");
        return $fetch_data;
    }


} // END OF CLASS
?>